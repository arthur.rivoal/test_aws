#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Manip résultat AIS

Created on Thu Oct 25 09:16:51 2018

@authors: Cyrile.Delestre, Arthur.Rivoal
"""
import numpy as np
import pandas as pd
from pandas.tseries.offsets import BDay
from datetime import datetime

thresh = 0.8
invest_by_day = 8e6
coeff = 100*100
horizons = [10]
print("Horizons d'investisements choisis : {}\n".format(horizons))

spread = 'ASSET_SWAP_SPD_MID' #spread de swap
yld = 'YLD_YTM_MID' #rendement

dossier = 'eval_results/'

raw_file = "AIS_data_1.csv"
eval_file = dossier + "eval_results_hybride.pkl"
print(eval_file)

############################################## FONCTIONS UTILITAIRES ################################################

def offsets(df, horizons):
    """ Décale les colonnes
    """
    for horiz in horizons:
        df['SPD_{}'.format(horiz)] = df[spread].shift(periods=-horiz)
        df['YLD_{}'.format(horiz)] = df[yld].shift(periods=-horiz)
    return df

def deltaT(df, dt=0):
    """ Retourne, en années (flottantes), la durée restante jusqu'à maturité
    """
    date = df.DATE+BDay(dt)
    nb_years = df.MATURITY.year-date.year
    rest_year = (datetime(date.year+1, 1, 1)-date).days
    frac_year = rest_year/(datetime(date.year+1,1,1)-datetime(date.year,1,1)).days
    return nb_years+frac_year

########################################### STRATEGIES D'INVESTISSEMENT #############################################

#1
def naive_StratReturns(df):
    """ Stratégie naïve : Investit la somme du jour j, répartie sur tous les ISINS.
    """
    frac = invest_by_day/df.shape[0]
    df['naiveReturns'] = df.apply(lambda x: frac*x[spread]/coeff*x.DeltaTJ, axis=1)
    return df

#4
def strat_max(df):
    """ Stratégie clairevoyante : utilise la target au lieu des probabilités de montées prédites.
    Si aucune ISIN ne monte, investit uniformément, le jour-même.
    """
    pred = df.loc[:,['target_{}'.format(horiz) for horiz in horizons]].values
    sum_pred = np.sum(pred[pred > 0])
    if sum_pred == 0:
        frac = invest_by_day/df.shape[0]
        df['maxReturns'] = df.apply(lambda x: frac*x[spread]/coeff*x.DeltaTJ, axis=1)
    else:
        frac = invest_by_day/sum_pred
        dT = df.loc[:,['DeltaTJ_{}'.format(horiz) for horiz in horizons]].values
        rend = df.loc[:,['SPD_{}'.format(horiz) for horiz in horizons]].values
        df['maxReturns'] = (pred > 0)*pred*frac*rend/coeff*dT
    return df

#5
def strat_min(df):
    pred = df.loc[:,['target_{}'.format(horiz) for horiz in horizons]].values
    pred = np.logical_not(pred)
    sum_pred = np.sum(pred[pred > 0])
    if sum_pred == 0:
        frac = invest_by_day/df.shape[0]
        df['minReturns'] = df.apply(lambda x: frac*x[spread]/coeff*x.DeltaTJ, axis=1)
    else:
        frac = invest_by_day/sum_pred
        dT = df.loc[:,['DeltaTJ_{}'.format(horiz) for horiz in horizons]].values
        rend = df.loc[:,['SPD_{}'.format(horiz) for horiz in horizons]].values
        df['minReturns'] = (pred > 0)*pred*frac*rend/coeff*dT
    return df

#2
def dl_StratReturns(df):
    """ A j, divise la somme entre les ISINs montantes et la dépose sur j+10 selon sa sûreté.
    Si aucune ne monte, investit uniformément sur les ISINs, le jour-même.
    """
    pred = df.loc[:,['pred_{}'.format(horiz) for horiz in horizons]].values
    sum_pred = np.sum(pred[pred > thresh])
    if sum_pred == 0:
        frac = invest_by_day/df.shape[0]
        df['quantReturns'] = df.apply(lambda x: frac*x[spread]/coeff*x.DeltaTJ, axis=1)
    else:
        frac = invest_by_day/sum_pred
        dT = df.loc[:,['DeltaTJ_{}'.format(horiz) for horiz in horizons]].values
        rend = df.loc[:,['SPD_{}'.format(horiz) for horiz in horizons]].values
        df['quantReturns'] = (pred > thresh)*pred*frac*rend/coeff*dT
    return df

################################################ CALCUL #############################################################

if __name__=="__main__":
    data_raw = pd.read_csv(raw_file, sep=';')
    data_eval_raw = pd.read_pickle(eval_file)
    
    # Convertit les dates en datetime
    # arrondit le spread et le rendement en bips entiers
    # Créé les colonnes spread et rendement décalées des horizons donnés
    data_raw.DATE = data_raw.DATE.map(lambda x: datetime.strptime(x,'%d/%m/%Y'))
    data_raw.MATURITY = data_raw.MATURITY.map(lambda x: datetime.strptime(x,'%d/%m/%Y'))
    data_raw[spread] = data_raw[spread].transform(np.floor)
    data_raw[yld] = data_raw[yld].map(lambda x : x*100)
    data_raw[yld] = data_raw[yld].transform(np.floor)
    data_raw = data_raw.set_index(keys='DATE', drop=True).\
                    groupby(by=['ISIN'], as_index=False).\
                        apply(lambda x: offsets(x, horizons)).\
                            reset_index(drop=False)
    # Joint les 2 dataframes 
    data_eval = pd.merge(left=data_eval_raw,
                         right=data_raw.loc[:,['DATE', 'ISIN', 'MATURITY', spread, yld]+\
                                            ['SPD_{}'.format(horiz) for horiz in horizons]+\
                                            ['YLD_{}'.format(horiz) for horiz in horizons]
                                            ],
                         how='left', 
                         on=['DATE', 'ISIN'],
                         left_index=False,
                         right_index=False)
    data_eval.dropna(inplace=True)
    
    # Ramène la proba entre -1 et 1
    data_eval[['pred_{}'.format(horiz) for horiz in horizons]] = \
    data_eval[['pred_{}'.format(horiz) for horiz in horizons]].apply(lambda x: (x-0.5)*2, axis=0) # threshold à 0.5
    
    # Rajoute la durée (en années) restant jusqu'à maturité de l'ISIN, et les durées en attentant l'investissement
    data_eval['DeltaTJ'] = data_eval.apply(lambda x: deltaT(x), axis=1)
    for horiz in horizons:
        data_eval['DeltaTJ_{}'.format(horiz)] = data_eval.apply(lambda x: deltaT(x, dt=horiz), axis=1)
    
    # Calcul des retours sur chaque stratégie
    data_eval = data_eval.groupby(by=['DATE'], as_index=False).apply(lambda x: naive_StratReturns(x)) #OK
    data_eval = data_eval.groupby(by=['DATE'], as_index=False).apply(lambda x: strat_max(x))
    data_eval = data_eval.groupby(by=['DATE'], as_index=False).apply(lambda x: strat_min(x))
    data_eval = data_eval.groupby(by=['DATE'], as_index=False).apply(lambda x: dl_StratReturns(x))
    
    def SharpeRatio(df, strat):
        df2 = df.copy()
        df2 = df2[df2[strat] !=0]
        numerator = np.mean(df2[['SPD_{}'.format(horiz) for horiz in horizons]].values)
        denominator = np.std(df2[['YLD_{}'.format(horiz) for horiz in horizons]].values)
        return(numerator/denominator)
    
    def bilanStrat(df, strat):
        totalInvest = invest_by_day*len(df['DATE'].unique().tolist())
        totalReturn = np.floor(df[strat].sum())
        propReturn = "{0:.2f}".format((totalReturn / totalInvest)*100)
        print("{} : {} € (abs) / {} % (prop)".format(strat, totalReturn, propReturn))
    
    bilanStrat(data_eval, 'naiveReturns')
    print("Ratio de Sharpe : {}".format(SharpeRatio(data_eval, "naiveReturns")))
    bilanStrat(data_eval, 'quantReturns')
    print("Ratio de Sharpe : {}".format(SharpeRatio(data_eval, "quantReturns")))
    bilanStrat(data_eval, 'minReturns')
    print("Ratio de Sharpe : {}".format(SharpeRatio(data_eval, "minReturns")))
    bilanStrat(data_eval, 'maxReturns')
    print("Ratio de Sharpe : {}".format(SharpeRatio(data_eval, "maxReturns")))


##3
#def strat_safe(df, frac):
#    pred = df.loc[['pred_{}'.format(horiz) for horiz in horizons]].values
#    sum_confi = np.sum(pred[pred > 0])
#    if sum_confi == 0:
#        return frac*df[yld]/coeff*df.DeltaTJ
#    else:
#        rep = frac/sum_confi
#        dT = df.loc[['DeltaTJ_{}'.format(horiz) for horiz in horizons]].values
#        spd = df.loc[['YLD_{}'.format(horiz) for horiz in horizons]].values
#        return np.sum((pred > 0)*pred*rep*spd/coeff*dT)
#
#def dl_StratReturns_safer(df):
#    frac = invest_by_day/df.shape[0]
#    df['dl_StratReturns_safer'] = df.apply(lambda x: strat_safe(x, frac=frac), axis=1)
#    return df