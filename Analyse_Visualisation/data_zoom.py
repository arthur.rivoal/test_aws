#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Nov 21 16:59:21 2018

@author: Arthur.Rivoal
"""
import random
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from testvar import *
import sklearn.preprocessing as prep
from multiprocessing import Pool
from functools import partial

scaler = prep.RobustScaler(quantile_range=(5.0, 95.0))

data = pd.read_csv('AIS_data_2.csv', sep = ';')
data_old = pd.read_csv('AIS_data_1.csv', sep = ';')

assert len(data.columns.tolist()) == len(data_old.columns.tolist())

spread = 'ASSET_SWAP_SPD_MID'
yld = 'YLD_YTM_MID'

target = spread

var = random.choice(keys)

data.DATE = pd.to_datetime(data.DATE, format= "%d/%m/%Y")
data['taux'] = data.apply(lambda x: x[yld]*100-x[spread], axis=1)
data[yld] = 100*data[yld]

def marketcurves():
    plt.figure()
    plt.margins(0.005,0.05)
    legend = []
    data_grp = data.groupby(by=['RATING'], as_index=False)
    color = ['C{}'.format(ii) for ii in range(1,10)]
    for nn, grp in enumerate(data_grp):
        rat,dat = grp
        for uu,ii in enumerate(dat.ISIN.unique()):
            plt.plot(dat[dat.ISIN==ii].DATE, dat[dat.ISIN==ii][yld], color=color[nn], label=rat, linewidth=0.7)
    plt.title('yield')
    
    #plt.legend(fontsize = 6)
    plt.figure()
    plt.margins(0.005,0.05)
    legend = []
    data_grp = data.groupby(by=['RATING'], as_index=False)
    color = ['C{}'.format(ii) for ii in range(1,10)]
    for nn, grp in enumerate(data_grp):
        rat,dat = grp
        for uu,ii in enumerate(dat.ISIN.unique()):
            plt.plot(dat[dat.ISIN==ii].DATE, dat[dat.ISIN==ii]['taux'], color=color[nn], label=rat, linewidth=0.7)
    plt.title('taux')
    
    plt.figure()
    plt.margins(0.005,0.05)
    legend = []
    data_grp = data.groupby(by=['RATING'], as_index=False)
    color = ['C{}'.format(ii) for ii in range(1,10)]
    for nn, grp in enumerate(data_grp):
        rat,dat = grp
        for uu,ii in enumerate(dat.ISIN.unique()):
            plt.plot(dat[dat.ISIN==ii].DATE, dat[dat.ISIN==ii][spread], color=color[nn], label=rat, linewidth=0.7)
    plt.title('spread')

marketcurves()

pb = ['FR0011780808', 'FR0011659366']
rq = ['IT0005105488'] # valeurs manquantes en Août

test = data[data.ISIN == var]
med = test.rolling(7, on = target)[target].median()
quantmin = test.rolling(7, on = target)[target].quantile(0.33)
quantmax = test.rolling(7, on = target)[target].quantile(0.67)
plt.figure()
plt.margins(0.005,0.05)
plt.plot(data[data.ISIN == var].DATE, data[data.ISIN == var][target], \
         linewidth=0.5, color = 'k', alpha = 0.5)
plt.plot(data[data.ISIN == var].DATE, med, linewidth=0.8, color = 'k')
plt.plot(data[data.ISIN == var].DATE, quantmin, linewidth=0.5)
plt.plot(data[data.ISIN == var].DATE, quantmax, linewidth=0.5)
plt.title(target)
#
#median = data.groupby('DATE')[spread].median()
#mean = data.groupby('DATE')[spread].mean()
#q1 = data.groupby('DATE')[spread].quantile(0.16) # sigma
#q3 = data.groupby('DATE')[spread].quantile(0.84) # sigma
#up = mean


#def homemade_scaling(X, transformer_X = None, mode = 'train', model_norm = scaler):
#    """Scaling data
#    X is a pd.DataFrame or a pd.Series
#    - Train mode sequentially fits and runs a transformer on X
#    - Test mode runs the transformer on X given said transformer
#    Outputs an ndarray.
#    
#    La fonction est vouée à changer pour une normalisation plus maline...
#    """
#    if isinstance(X, pd.Series):
#        X = X.values.reshape(-1,1)
#    else:
#        assert isinstance(X, pd.DataFrame)
#    if mode == 'train':
#        transformer_X = model_norm.fit(X)
#        X = transformer_X.transform(X)
#        return X, transformer_X
#    else:
#        X = transformer_X.transform(X)
#        return X
#
#def multi_fun(key, train, test, col, to_be_scaled):
#    """ Doc TBC
#    """
#    df_train = train.loc[train[col]==key, :]
#    df_train.loc[:, to_be_scaled], transformer = homemade_scaling(df_train.loc[:, to_be_scaled])
#    df_test = test.loc[test[col]==key, :]
#    df_test.loc[:, to_be_scaled] = homemade_scaling(df_test.loc[:,to_be_scaled], transformer, mode='test')
#    return df_train, df_test
#
#with Pool(processes=4) as pool:
#    fun_pool = partial(multi_fun, train=train, test=test, col=['ISIN'], to_be_scaled=spread)
#    res = pool.map(fun_pool, data['ISIN'].unique().tolist())
#    list_train, list_test = list(zip(*res))
#    train = pd.concat(list_train, ignore_index=True)
#    test = pd.concat(list_test, ignore_index=True)
