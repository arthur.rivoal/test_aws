#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Nov  9 14:52:26 2018

@author: Arthur.Rivoal
"""
import numpy as np
import pickle
import matplotlib.pyplot as plt
import matplotlib.colors as colors
#from matplotlib.mlab import bivariate_normal

horizons = [10]

with open('images(9).pkl', 'rb') as file:
    total = pickle.load(file)

fmap = total.pop()
fmap += ['Phi_0']

class MidpointNormalize(colors.Normalize):
    def __init__(self, vmin=None, vmax=None, midpoint=None, clip=False):
        self.midpoint = midpoint
        colors.Normalize.__init__(self, vmin, vmax, clip)

    def __call__(self, value, clip=None):
        # I'm ignoring masked values and all kinds of edge cases to make a
        # simple example...
        x, y = [self.vmin, self.midpoint, self.vmax], [0, 0.5, 1]
        return np.ma.masked_array(np.interp(value, x, y))


def plot_image(cm, classes=fmap, title='\o/', cmap=plt.cm.seismic):
    """
    This function prints and formats the confusion matrix for plotting.
    Normalization can be applied by setting `normalize=True`.
    """
    cm = np.array(cm)
    mid_val=0
    plt.imshow(cm.transpose(), cmap=cmap, clim=(cm.min(), cm.max()), norm=\
               MidpointNormalize(midpoint=mid_val,vmin=cm.min(), vmax=cm.max()))
    plt.colorbar()
    plt.title(title, weight = 'bold', fontsize = 10)
    tick_marks = np.arange(len(classes))
    plt.yticks(tick_marks, classes, fontsize = 6)
    plt.ylabel(' ', fontsize = 10)
    plt.xlabel('Business Days (from 01/01/2018)', fontsize = 10)

for i in range(len(horizons)):
    image = total[i]
    plt.figure()
    plot_image(total[i], title = 'Horizon : {}'.format(horizons[i]))
    plt.show()
    
