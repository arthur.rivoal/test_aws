#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Manip résultat AIS

Created on Thu Oct 25 09:16:51 2018

@author: Cyrile.Delestre
"""
import numpy as np
import pandas as pd
from pandas.tseries.offsets import BDay
from datetime import datetime, timedelta
from dateutil.relativedelta import relativedelta

invest_by_day = 8e6

if __name__=="__main__":
    data_raw = pd.read_csv("AIS_data_1.csv", sep=';')
    data_eval_raw = pd.read_pickle("eval_results.pkl")
    
    data_raw.DATE = data_raw.DATE.map(lambda x: datetime.strptime(x,'%d/%m/%Y'))
    data_raw.MATURITY = data_raw.MATURITY.map(lambda x: datetime.strptime(x,'%d/%m/%Y'))
    data_raw.ASSET_SWAP_SPD_MID = data_raw.ASSET_SWAP_SPD_MID.transform(np.floor)
    
    def offset_pred(df):
        df['SPD_5'] = df.ASSET_SWAP_SPD_MID.shift(periods=-5)
        df['SPD_10'] = df.ASSET_SWAP_SPD_MID.shift(periods=-10)
        df['SPD_15'] = df.ASSET_SWAP_SPD_MID.shift(periods=-15)
        df['SPD_20'] = df.ASSET_SWAP_SPD_MID.shift(periods=-20)
        return df
    
    data_raw = data_raw.set_index(keys='DATE', drop=True).\
                    groupby(by=['ISIN'], as_index=False).\
                        apply(lambda x: offset_pred(x)).\
                            reset_index(drop=False)
                            
    data_eval = pd.merge(left=data_eval_raw,
                         right=data_raw.loc[:,['DATE',
                                               'ISIN',
                                               'MATURITY',
                                               'ASSET_SWAP_SPD_MID',
                                               'SPD_5',
                                               'SPD_10',
                                               'SPD_15',
                                               'SPD_20']],
                         how='left', 
                         on=['DATE', 'ISIN'],
                         left_index=False,
                         right_index=False)
    
    data_eval.dropna(inplace=True)
    
    data_eval[['pred_5', 'pred_10', 'pred_15', 'pred_20']] = data_eval[['pred_5', 'pred_10', 'pred_15', 'pred_20']].apply(lambda x: (x-0.5)*2, axis=0)
    
    def deltaT(df, dt=0):
        date = df.DATE+BDay(dt)
        nb_years = df.MATURITY.year-date.year
        rest_year = (datetime(date.year+1, 1, 1)-date).days
        frac_year = rest_year/(datetime(date.year+1,1,1)-datetime(date.year,1,1)).days
        return nb_years+frac_year
    
    data_eval['DeltaTJ'] = data_eval.apply(lambda x: deltaT(x), axis=1)
    data_eval['DeltaTJ_5'] = data_eval.apply(lambda x: deltaT(x, dt=5), axis=1)
    data_eval['DeltaTJ_10'] = data_eval.apply(lambda x: deltaT(x, dt=10), axis=1)
    data_eval['DeltaTJ_15'] = data_eval.apply(lambda x: deltaT(x, dt=15), axis=1)
    data_eval['DeltaTJ_20'] = data_eval.apply(lambda x: deltaT(x, dt=20), axis=1)
    
    data_eval['DBips_5'] = data_eval.ASSET_SWAP_SPD_MID-data_eval.SPD_5
    data_eval['DBips_10'] = data_eval.ASSET_SWAP_SPD_MID-data_eval.SPD_10
    data_eval['DBips_15'] = data_eval.ASSET_SWAP_SPD_MID-data_eval.SPD_15
    data_eval['DBips_20'] = data_eval.ASSET_SWAP_SPD_MID-data_eval.SPD_20
    
    def candide_strat(df):
        nb_frac = invest_by_day/df.shape[0]
        df['candide_strat'] = df.apply(lambda x: nb_frac*x.ASSET_SWAP_SPD_MID/10000*x.DeltaTJ, axis=1)
        return df
    
    data_eval = data_eval.groupby(by=['DATE'], as_index=False).apply(lambda x: candide_strat(x))
    
    def strat_risky(df, nb_frac):
        pred = df.loc[['pred_5', 'pred_10', 'pred_15', 'pred_20']].values
        dT = df.loc[['DeltaTJ_5', 'DeltaTJ_10', 'DeltaTJ_15', 'DeltaTJ_20']].values
        spd = df.loc[['SPD_5', 'SPD_10', 'SPD_15', 'SPD_20']].values
        return np.sum((pred > 0)*pred*nb_frac*spd/10000*dT)
    
    def datalabs_risky_strat(df):
        confi = df.loc[:,['pred_5', 'pred_10', 'pred_15', 'pred_20']].values
        sum_confi = np.sum(confi[confi > 0])
        if sum_confi == 0:
            nb_frac = invest_by_day/df.shape[0]
            df['datalabs_risky_strat'] = df.apply(lambda x: nb_frac*x.ASSET_SWAP_SPD_MID/10000*x.DeltaTJ, axis=1)
        else:
            nb_frac = invest_by_day/sum_confi
            df['datalabs_risky_strat'] = df.apply(lambda x: strat_risky(x, nb_frac=nb_frac), axis=1)
        return df
    
    data_eval = data_eval.groupby(by=['DATE'], as_index=False).apply(lambda x: datalabs_risky_strat(x))
    
    def strat_safe(df, nb_frac):
        pred = df.loc[['pred_5', 'pred_10', 'pred_15', 'pred_20']].values
        sum_confi = np.sum(pred[pred > 0])
        if sum_confi == 0:
            return nb_frac*df.ASSET_SWAP_SPD_MID/10000*df.DeltaTJ
        else:
            rep = nb_frac/sum_confi
            dT = df.loc[['DeltaTJ_5', 'DeltaTJ_10', 'DeltaTJ_15', 'DeltaTJ_20']].values
            spd = df.loc[['SPD_5', 'SPD_10', 'SPD_15', 'SPD_20']].values
            return np.sum((pred > 0)*pred*rep*spd/10000*dT)
      
    def datalabs_safe_strat(df):
        nb_frac = invest_by_day/df.shape[0]
        df['datalabs_safe_strat'] = df.apply(lambda x: strat_safe(x, nb_frac=nb_frac), axis=1)
        return df
    
    data_eval = data_eval.groupby(by=['DATE'], as_index=False).apply(lambda x: datalabs_safe_strat(x))
    
    print("ROI candide strat : {}€".format(np.floor(data_eval.candide_strat.sum())))
    print("ROI risky strat : {}€".format(np.floor(data_eval.datalabs_risky_strat.sum())))
    print("ROI safe strat : {}€".format(np.floor(data_eval.datalabs_safe_strat.sum())))
