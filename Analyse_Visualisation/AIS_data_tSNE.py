#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Nov 15 09:33:06 2018

@author: Arthur.Rivoal
"""
import random
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.manifold import TSNE

raw = False
not_raw = 'output'
dirc = 'tSNE/'

nb_by_cl = 250 # nombre d'image par carachtère
recal = False
nb_im_iter = 100 # Nombre d'image par itération pour les CNN
target = 'ASSET_SWAP_SPD_MID'

if __name__=='__main__':

    if raw :
        data = pd.read_csv('AIS_data_1.csv', sep = ';')
        # Create target
        data['label'] = data.groupby('ISIN', as_index = False).apply(lambda x: \
            x[target].shift(-15)-x[target]>0).reset_index(drop = True)
        # TBC
    else :
        if not_raw == 'input':
            data = pd.read_pickle('train(2).pkl')
            labels = data['target_10']
            data.drop('target_10', axis = 1, inplace = True)
            data.drop('ISIN', axis = 1, inplace = True)
            data.drop('DATE', axis = 1, inplace = True)
        if not_raw == 'output':
            data = pd.read_pickle(dirc+'tSNE_train(8).pkl') # 3 pour first avec bnorm; 5 pour first sans bnorm
            labels = data.label
            data = data.drop(columns = 'label')
    
    classes = [True, False]
    
    reduce_tsne_2d = TSNE(n_components=2,
                          init='pca',
                          learning_rate=350,
                          perplexity=40,
                          early_exaggeration=10,
                          verbose=2,
                          random_state=42,
                          n_iter=500,
                          n_iter_without_progress=50,
                          method='barnes_hut')

    X_embedded = reduce_tsne_2d.fit_transform(data)
    X_embeddedDF = pd.DataFrame({'x': X_embedded[:,0], 'y': X_embedded[:,1]})
    X = pd.concat([X_embeddedDF, labels], axis = 1)

    plt.figure()
    colors = np.divide(random.sample(range(10),len(classes)), 10)
    colors = ['b', 'r']
    for i,cl in enumerate(classes) :
        plt.scatter(X[X['label'] == cl].x,
                    X[X['label'] == cl].y,
                    c=colors[i],
                    alpha=0.6,
                    label=cl,
                    edgecolors='none')
    plt.legend()
