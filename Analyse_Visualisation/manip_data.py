# -*- coding: utf-8 -*-
"""
Script manip set

Created on Sat Oct 13 00:01:15 2018

@author: Cyrile
"""

import DataBase as db
import pandas as pd
import numpy as np 
import sklearn.preprocessing as prepro
import json
from datetime import datetime, timezone
from functools import reduce

if __name__=='__main__':
    db_news = db.DataBase('news.db', compress=False)
    
    # Univers provider subjects et audiences
#    news = db_news("SELECT * FROM data")
#    news.drop(labels=['time', 'sourceTimestamp', 'sourceId', 'headline','headlineTag', 'assetName'], inplace=True, axis=1)
#    
#    univ_provider = news.provider.unique().tolist()
#    
#    def add_set(x):
#        return reduce(lambda a, b: a.union(eval(b)), x, set())
#    
#    univ_subjects = list(add_set(news.subjects))
#    univ_audiences = list(add_set(news.audiences))
#    
#    dic = {"univ_provider": univ_provider,
#           "univ_subjects": univ_subjects,
#           "univ_audiences": univ_audiences}
#    
#    str_json = json.dumps(dic)
#    with open('univ.json', 'w') as file:
#        file.write(str_json)
    
    # Construction du date set
    print('Contruction du data set news')
    date_limite = datetime.strptime('Jan 1 2016', '%b %d %Y')
    date_limite = date_limite.replace(tzinfo=timezone.utc)
    news = db_news("SELECT * FROM data WHERE firstCreated >= {}".format(int(date_limite.timestamp())))
    news.drop(labels=['time', 'sourceTimestamp', 'sourceId', 'headline','headlineTag', 'assetName'], inplace=True, axis=1)
    
    with open('univ.json', 'r') as file:
        dic = json.loads(file.readline())
        
    u_provider = list(map(lambda x: 'pro_'+x, dic["univ_provider"]))
    u_audiences = list(map(lambda x: 'aud_'+x, dic["univ_audiences"]))
    
    df_prov = pd.DataFrame(0, index=np.arange(news.shape[0]), columns=u_provider)
    df_aud = pd.DataFrame(0, index=np.arange(news.shape[0]), columns=u_audiences)
    
    news = pd.concat([news, df_prov, df_aud], axis=1, ignore_index=False, copy=False)
    
    def fun_onehot(x, var, univ, prefix):
        sing = prefix+x[var]
        if sing in univ:
            x[sing] = 1
        return x
    
    def fun_onehot_on_set(x, var, univ, prefix):
        list_ = map(lambda y: prefix+y, list(eval(x[var])))
        for ii in  list_:
            if ii in univ:
                x[ii] = 1
        return x
    
    def fun_agg(x, var1, var2, univ1, univ2, prefix1, prefix2):
        x = fun_onehot(x, var1, univ1, prefix1)
        x = fun_onehot_on_set(x, var2, univ2, prefix2)
        return x
            
    news = news.apply(fun_agg,
                      axis=1,
                      result_type='broadcast',
                      var1='provider',
                      var2='audiences',
                      univ1=u_provider,
                      univ2=u_audiences,
                      prefix1='pro_',
                      prefix2='aud_')
    
    # Jointure
    print('Jointure')
    market = pd.read_pickle('market_train.pkl')
    market = market[market.time >= date_limite]
    market['date'] = market.time.map(lambda x: x.date())
    news.firstCreated = news.firstCreated.map(lambda x: datetime.utcfromtimestamp(x))
    news['date'] = news.firstCreated.map(lambda x: x.date())
    
    news['nbAssetCodes'] = news.assetCodes.map(lambda x: len(eval(x)))
    news = news.loc[np.repeat(news.index.values, news.nbAssetCodes)]
    news.reset_index(drop=False, inplace=True)
    
    def fun(x):
        x.loc[:,'index'] = np.arange(x.shape[0])
        return x
    
    news = news.groupby(by='index', as_index=False).apply(lambda x: fun(x))
    news['assetCode'] = news.apply(lambda x: list(eval(x.assetCodes))[x['index']], axis=1)
    news = news.assign(nb_news=np.ones(news.shape[0]))
    news.drop(labels=['index', 'nbAssetCodes', 'provider', 'subjects','audiences', 'assetCodes'], inplace=True, axis=1)
    
    for ii in news.columns:
        if ii not in ['firstCreated', 'date', 'assetCode']:
            news[ii] = news[ii].astype(float)
    
    news_util = news.groupby(by=['date', 'assetCode'], as_index=False).sum()
    
    final_data_set = pd.merge(left=market, right=news_util, how='left', left_on=['date', 'assetCode'], right_on=['date', 'assetCode'], copy=False)
    
    final_data_set.nb_news.fillna(0, inplace=True)
    final_data_set.to_pickle("data_set.pkl")
