#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jan  3 11:40:31 2019

@author: Arthur.Rivoal
"""
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import gridspec
from sklearn import metrics
from datetime import datetime
import itertools

################################


dossier = 'eval_results/'
res = pd.read_pickle(dossier+'eval_results_hybridebn.pkl')
suite = pd.read_pickle(dossier+'eval_results_bn_suite.pkl')
suite.sort_values(by=['DATE'], inplace = True)

res.append(suite, ignore_index = True)
res.sort_values(by=['DATE'], inplace = True)
