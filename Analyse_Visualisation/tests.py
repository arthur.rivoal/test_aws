#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Oct 24 14:41:50 2018

@author: Arthur.Rivoal
"""

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import gridspec
from sklearn import metrics
from datetime import datetime
import itertools

################################### FICHIER ####################################
singlefile = 'no'

if singlefile == 'yes':
    dossier = 'eval_results/'
    res = pd.read_pickle(dossier+'eval_results_bn_suite.pkl')
    assert np.sum(res.isna().sum().values) == 0
    res.sort_values(by=['DATE'], inplace = True)
else:
    dossier = 'eval_results/'
    res = pd.read_pickle(dossier+'eval_results_hybridebn.pkl')
    suite = pd.read_pickle(dossier+'eval_results_bn_suite.pkl')
    suite.sort_values(by=['DATE'], inplace = True)
    res = res.append(suite, ignore_index = True)
    res.sort_values(by=['DATE'], inplace = True)


recalage = True
threshold = 0.5

numMonths = 10
year = 2018

horizon = 10

jours_pourris = []
# BDays count :
if recalage == True:
    import calendar 
    weekday_count = 0
    cal = calendar.Calendar()
    for week in cal.monthdayscalendar(year, numMonths+1):
        for i, day in enumerate(week):
            if day == 0 or i >= 5:
                continue
            weekday_count += 1

monthranges = [calendar.monthrange(year, month)[1] for month in range(1,numMonths+1)]

################################# FONCTIONS ####################################

def filtre(df, mo):
    """ Sélectionne les lignes correspondant au mois spécifié
    """
    date = datetime(2018, mo, 1)
    df_ = df.loc[(df['DATE'].dt.month==date.month)&\
                         (df['DATE'].dt.year==date.year)]
    return df_

def map_bool(df, col, tresh):
    """ Maps probability to binary depending on threshold
    """
    df[col] = df[col].map(lambda x: x>=tresh)
    return df

def acc_score(y_pred, y_test, tresh=0.5):
    """ Calcule le pourcentage de prédictions justes
    """
    y_p = np.array(list(map(int, y_pred >= tresh)))
    return [np.sum(y_test == y_p)/len(y_pred), len(y_pred)]

def plot_confusion_matrix(cm, classes=[u'\u2193', u'\u2191'],
                          normalize=False,
                          title='',
                          cmap=plt.cm.binary):
    """
    This function prints and formats the confusion matrix for plotting.
    Normalization can be applied by setting `normalize=True`.
    """
    my_colors = ['r','g']
    plt.title(title, weight = 'bold', fontsize = 10)
    if normalize:
        cm = cm.astype('float') / cm.sum(axis=0)[np.newaxis, :]
        print("Normalized confusion matrix (columns)")
        cmap = plt.cm.binary
    else:
        print('Confusion matrix, without normalization')
    tick_marks = np.arange(len(classes))
    plt.xticks(tick_marks, classes, fontsize = 12)
    plt.yticks(tick_marks, classes, fontsize = 12)
    for ticklabel, tickcolor in zip(plt.gca().get_xticklabels(), my_colors):
        ticklabel.set_color(tickcolor)
    for ticklabel, tickcolor in zip(plt.gca().get_yticklabels(), my_colors):
        ticklabel.set_color(tickcolor)
    print(cm)
    plt.imshow(cm, interpolation='nearest', cmap=cmap, alpha = 0.4)
    fmt = '.2f' if normalize else 'd'
    for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
        plt.text(j, i, format(cm[i, j], fmt),
                 horizontalalignment="center",
                 fontsize = 12,
                 weight = 'bold',
                 color="black")

def blabla(df, horiz, confs=[], accs=[]):
    """ blabla fait le café.
    blabla prend le df results et créé deux listes de confm et d'acc, par mois,
    ainsi que pour la totalité de la période (en dernière position).
    """
    for i in range(1,numMonths+1):
        df2 = filtre(df, i)
        accus = acc_score(df2['pred_{}'.format(horiz)].values,\
                          df2['target_{}'.format(horiz)].values,\
                          tresh=threshold)
        accs.append(accus)
        confm =\
        metrics.confusion_matrix(df2['target_{}'.format(horiz)],\
                                map_bool(df2,'pred_{}'.format(horiz),\
                                         threshold)['pred_{}'.format(horiz)])
        confs.append(confm)
    df2 = df.copy()
    accus = acc_score(df2['pred_{}'.format(horiz)].values,\
                      df2['target_{}'.format(horiz)].values,\
                      tresh=threshold)
    accs.append(accus)
    confm =\
    metrics.confusion_matrix(df2['target_{}'.format(horiz)],\
                            map_bool(df2,'pred_{}'.format(horiz),\
                                     threshold)['pred_{}'.format(horiz)])
    confs.append(confm)
    return confs, accs

############################### DASHBOARDING 1 #################################

confusion_matrices, accuracies = blabla(res, horizon)

res = map_bool(res, 'pred_{}'.format(horizon), threshold)
b = res.groupby(by='DATE').apply(lambda group : \
             acc_score(group['pred_{}'.format(horizon)], group['target_{}'.format(horizon)]))
prop = res.groupby(by='DATE').apply(lambda group : \
                group['target_{}'.format(horizon)].sum()/len(group['target_{}'.format(horizon)]))
prop_pred = res.groupby(by='DATE').apply(lambda group : \
                     group['pred_{}'.format(horizon)].sum()/len(group['pred_{}'.format(horizon)]))

if recalage == True:
    b.reset_index(drop = True, inplace = True)

time_line = b.index.values
c,_ = zip(*b.values)
c_zip = list(zip(time_line, c))
c_sup = filter(lambda x: x[1] >= 0.5, c_zip)
c_inf = filter(lambda x: x[1] < 0.5, c_zip)

tl_sup, c_sup = zip(*c_sup)
try:
    tl_inf, c_inf = zip(*c_inf)
except:
    pass

############################### DASHBOARDING 2 #################################
if recalage == True:
    listeMois = list(range(weekday_count))
else:
    listeMois = [datetime(2018,i,1) for i in range(1, numMonths+1)]

gs = gridspec.GridSpec(5, numMonths+1) 
fig = plt.figure()

""" Plot de la précision journalière """
plt.subplot(gs[:2, :numMonths+1])
plt.margins(0.005,0.05)
plt.title('Horizon {} \n '.format(horizon), weight = 'bold', fontsize = 14)
plt.ylabel('Précision journalière %')
plt.stem(tl_sup, c_sup, linefmt='g-', basefmt="gray", markerfmt=' ', bottom=0.5)
try:
    plt.stem(tl_inf, c_inf, linefmt='r-', basefmt="gray", markerfmt=' ', bottom=0.5)
except:
    pass
plt.tick_params(axis='x',          # changes apply to the x-axis
                which='both',      # both major and minor ticks are affected
                direction = 'in',
                bottom=True,      # ticks along the bottom edge are off
                top=False,         # ticks along the top edge are off
                labelbottom=True) # labels along the bottom edge are off
plt.hlines(0.6,time_line.min(), time_line.max(), color='gray', linewidth=0.5)
#for i in range(1,len(listeMois)):
#    plt.vlines(listeMois[i], 0 ,1, color='gray', linewidth=0.5)

""" Plot des % montées de Spread """
props = plt.subplot(gs[2, :numMonths+1])
plt.margins(0.005,0.05)
plt.ylabel('Montées %')
plt.plot(time_line, prop, color='k', alpha=1, linewidth=0.5)
plt.plot(time_line, prop_pred, color='c', linewidth=0.5)
plt.tick_params(axis='x',          # changes apply to the x-axis
                which='both',      # both major and minor ticks are affected
                direction = 'in',
                bottom=False,      # ticks along the bottom edge are off
                top=True,         # ticks along the top edge are off
                labelbottom=False) # labels along the bottom edge are off
plt.hlines(0.5,time_line.min(), time_line.max(), color='gray', linewidth=0.5)
plt.legend(['Observation','Modèle'])
#for i in range(1,len(listeMois)):
#    plt.vlines(listeMois[i], 0 ,1, color='gray', linewidth=0.5)

for i in range(numMonths):
    partial = plt.subplot(gs[3, i])
    if i==0:
        plt.ylabel('Observations\n(points)', fontsize = 10)
    plt.tick_params(
        axis='x',          # changes apply to the x-axis
        which='both',      # both major and minor ticks are affected
        bottom=False,      # ticks along the bottom edge are off
        top=False,         # ticks along the top edge are off
        labelbottom=False) # labels along the bottom edge are off
    plot_confusion_matrix(confusion_matrices[i])
    partial2 = plt.subplot(gs[4, i])
    if i==0:
        plt.ylabel('Observations', fontsize = 10)
    plt.xlabel('Prédictions\n(normalisées)', fontsize = 10)
    plot_confusion_matrix(confusion_matrices[i], normalize = True, \
                          title = 'Month {} : {}'.format(i+1, \
                                              round(accuracies[i][0], 3)))

plt.subplot(gs[3, numMonths])
plt.tick_params(
    axis='x',          # changes apply to the x-axis
    which='both',      # both major and minor ticks are affected
    bottom=False,      # ticks along the bottom edge are off
    top=False,         # ticks along the top edge are off
    labelbottom=False) # labels along the bottom edge are off
plot_confusion_matrix(confusion_matrices[-1])
plt.subplot(gs[4, numMonths])
plot_confusion_matrix(confusion_matrices[-1], normalize = True, \
                      title = 'Total : {}'.format(\
                                              round(accuracies[-1][0], 3)))
plt.xlabel('Prédictions', fontsize = 10)
plt.show()


#################################### SHAP Analysis #############################

def SHAP():
    
    data_shapley = pd.read_csv("data_shapley_1_15-v9.csv", header='infer', sep=';')
    feat_name = data_shapley.columns.tolist()
    extract = data_shapley.iloc[22,:].values
    name_col = list(zip(extract, feat_name))
    sort_val_biaised = sorted(name_col, key=lambda x: abs(x[0]), reverse=True)
    
    return sort_val_biaised






#
#
#import random
#from datetime import datetime
#import pandas as pd
#from pandas.tseries.offsets import BDay
#
#experiment_path = ''
#
#horizon = 20
#
#X_train = pd.read_pickle(experiment_path + 'train.pkl')
#target = 'target_'+str(horizon)
#
## On exclue les données futures
#X_ = X_train[X_train.DATE <= (X_train.DATE.max()-BDay(horizon))].copy()
#
## On veut aller chercher des dates au hasard pour le tirage de Test
#dateList = X_.DATE.unique().tolist()
#dateList = [datetime.utcfromtimestamp(int(i)/1e9) for i in dateList]
#
## On veut de toute façon apprendre sur les données de marché les plus récentes.
#for i in range(-horizon, 0): dateList.pop(i)
#nbTirages = 7
#offset = 3
#
## On veut trouver x tq : horiz*(nbTirages-1) + (x-1) = len(dateList)-1
#horiz = horizon + offset
#x = len(dateList) - horiz*(nbTirages-1)
#dateIndexSamples = []
#badDateIndex = []
#for i, j in enumerate(sorted(random.sample(range(horizon, x), nbTirages))):
#    pouet = horiz*i+j
#    for k in range(offset):
#        dateIndexSamples.append(pouet+k)
#    for l in range(1, horizon):
#        badDateIndex.append(pouet-l)
#dateSamples = [dateList[i] for i in dateIndexSamples]
#badDateSamples = [dateList[i] for i in badDateIndex]
#badDateSamples.extend(dateSamples)
#
#X_train = X_[~X_['DATE'].isin(badDateSamples)]
#X_group = X_train.groupby('target_{}'.format(horizon), as_index=False)
#X_train = X_group.apply(lambda group :\
#                        group.sample(X_group.size().min()).reset_index(drop = True))
#
#X_test = X_[X_['DATE'].isin(dateSamples)]
#X_test = X_test.sample(frac=1).reset_index(drop = True) # Shuffle
#
#
#########################
##     VERIFICATION     #
#########################
#
#toto = sorted([datetime.utcfromtimestamp(int(i)/1e9) \
#               for i in X_train.DATE.unique().tolist()])
#
#titi = sorted([datetime.utcfromtimestamp(int(i)/1e9) \
#               for i in X_test.DATE.unique().tolist()])
#
#for i in badDateSamples:
#    assert i not in toto
#
#assert len(dateSamples) == offset * nbTirages
#assert len(badDateSamples) == (offset+horizon-1) * nbTirages
#assert titi == dateSamples
#
####################### ANALYSE : virer à posteriori
#
#plt.figure()
#for tutu in [5,10,15,20]:
#    plt.ylabel('Montées %')
#    prop = X_train.groupby(by='DATE').apply(lambda group : \
#                          group['target_{}'.format(tutu)].sum()/ len(group['target_{}'.format(tutu)]))
#    time_line = prop.index.values
#    plt.plot(time_line, prop, color='k', alpha=1, linewidth=0.5)
#plt.show()
#
#

#df = prop.copy()
#df = df.reset_index()
#df = df.drop('DATE', axis = 1)
#df2 = prop_pred.copy()
#df2 = df2.reset_index()
#df2 = df2.drop('DATE', axis = 1)

#for horiz in range(25,40):
#    df = pd.concat([df, df2.shift(horiz)], axis = 1, ignore_index = True)
#    df.dropna(inplace = True)
#pd.scatter_matrix(df)

#### LAG PLOT ET AUTOCORRELATION (Pearson) PLOT

#plt.figure()
#lag_plot(prop)
#lag_plot(prop_pred)
#plt.figure()
#autocorrelation_plot(prop)
#autocorrelation_plot(prop_pred)


#plt.figure()
#plt.plot(time_line, c, 'o-')
#plt.plot([time_line[0], time_line[-1]], [0.5,0.5], 'r--')
#plt.title('Horizon {}'.format(horiz))
#plt.ylabel('Perfs')
#plt.xlabel('Temps')
#
#plt.figure()
#plt.hist(c, bins='auto', density=False, rwidth=0.8)
#plt.title('Horizon {}'.format(horiz))
#plt.ylabel('Fréquence')
#plt.xlabel('Perfs')

######## EXAMPLE LOCAL de code pour la feature importance

#list_name = pd.read_csv('fmap_20.txt', sep='\t', names=['idx','name','q']).name.values.tolist()
#mapper = {'f{}'.format(nn): ii for nn,ii in enumerate(list_name)}
#
#fig, ax = plt.subplots(figsize=(10, 20))
#model = xgb.Booster(model_file='xgb_model_20.dat')
#score = model.get_score(importance_type='weight')
#sort_sc = sorted(score.items(), key=operator.itemgetter(1), reverse=True)
#cax = ax.barh(range(len(sort_sc[:30])), list(map(lambda x: x[1], sort_sc[:30])), align='center')
#plt.yticks(range(len(sort_sc[:30])), list(map(lambda x: mapper[x[0]], sort_sc[:30])))
#plt.setp(ax.get_yticklabels(),
#         rotation=0,
#         ha="right",
#         rotation_mode="anchor")
#plt.title('Feature importance (number of split)')

####### EXAMPLE 2 de code pour la feature importance en ligne

#list_name = pd.read_csv('Data_Arthur/2/14/fmap_5.txt', sep='\t', names=['idx','name','q']).name.values.tolist()
#mapper = {'f{}'.format(nn): ii for nn,ii in enumerate(list_name)}
#
#fig, ax = plt.subplots(figsize=(10, 20))
#model = xgb.Booster(model_file='Data_Arthur/2/14/xgb_model_5.dat')
#score = model.get_score(importance_type='weight')
#sort_sc = sorted(score.items(), key=operator.itemgetter(1), reverse=True)
#cax = ax.barh(range(len(sort_sc[:])), list(map(lambda x: x[1], sort_sc[:])), align='center')
#plt.yticks(range(len(sort_sc[:])), list(map(lambda x: mapper[x[0]], sort_sc[:])))
#plt.setp(ax.get_yticklabels(),
#         rotation=0,
#         ha="right",
#         rotation_mode="anchor")
#plt.title('Feature importance (number of split)')
